CC = gcc
CCFLAGS = -Wall -std=gnu99 -pedantic -lpthread
DEBUGFLAGS = -g
EXE = simulator
RELEASEFLAGS = -O3
SOURCES = simulator.c shed.c

debug: CCFLAGS += $(DEBUGFLAGS)
debug: $(EXE)

release: CCFLAGS += $(RELEASEFLAGS)
release: $(EXE)

$(EXE): $(SOURCES)
	$(CC) -o $(EXE) $(CCFLAGS) $(SOURCES)

clean:
	rm -f $(EXE)

