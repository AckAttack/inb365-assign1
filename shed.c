//

#include <stdlib.h>
#include "shed.h"


int appendSheep(Shed * shed, Sheep * sheep) {
	if(shed->length >= MAX_SHEEP) {
		return SHED_FULL;
	}

	shed->pens[shed->length++] = sheep;
	return shed->length-1;
}

Sheep * getSheep(Shed * shed, unsigned int index) {
    return shed->pens[index];
}

Sheep * popSheep(Shed * shed, unsigned int index) {
	Sheep * toReturn = NULL;
		
	if(index >= shed->length) {
		return NULL;
	}

	toReturn = shed->pens[index];
	shed->length--;
	for(int i = index; i < shed->length; ++i) {
		shed->pens[i] = shed->pens[i + 1];
	}
	return toReturn;
}
