//

#ifndef SHED_H
#define SHED_H

#include <sys/time.h>
#include <time.h>

#define MAX_SHEEP 10

//Define error codes
#define SHED_FULL -1

/**
 * Represents a sheep that has entered the shed.
 *
 * char * id: a six character unique id
 * struct timeval entered: the time that the sheep entered the shed
 */
typedef struct {
	char * id;
	struct timeval entered;
} Sheep;

/**
 * The shed stores its sheep in an array with limited vector-like access
 * provided by functions in this file.
 *
 * unsigned int length: the number of sheep currently in the shed
 * Sheep * pens[]: storage for the sheep
 */
typedef struct {
	unsigned int length;
	Sheep * pens[MAX_SHEEP]; //Array of Sheep*
} Shed;

/*
 * Adds the given sheep to the end of the list
 *
 * Shed * shed: the Shed to which the sheep will be added
 * Sheep * sheep: the Sheep that will be added to the shed
 * returns: the pen that the sheep was put in, or SHED_FULL if the shed is full
 */
int appendSheep(Shed * shed, Sheep * sheep);

/**
 * Returns the sheep at the given position in the shed
 *
 * Shed * shed: the shed from which to get the sheep
 * unsigned int index: the index of the holding pen from which to get the sheep
 * returns: the sheep that is in the holding pen
 */
Sheep * getSheep(Shed * shed, unsigned int index);

/**
 * Removes and returns the sheep at the given position from the shed,
 * and moves the other sheep to fill in the gap, if necessary
 * 
 * Shed * shed: the shed from which to get the sheep
 * unsigned int index: the index of the holding pen from which to get the sheep
 * returns: the sheep that was in the holding pen
 */
Sheep * popSheep(Shed * shed, unsigned int index);

#endif

