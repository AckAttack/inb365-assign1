//

#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include "shed.h"

/**
 * There is a chance that this function will try to take a sheep out of the
 * shed every SLEEP_TIME microseconds.
 * If it succeeds, it prints a message stating how long the sheep was in the shed.
 * It loops until the global variable running is set to false.
 *
 * void * args: a pointer to a double that represents the chance of removing
 * 				a sheep.
 * returns: NULL
 */
void * consume(void * args);

/**
 * This function displays a greeting and description of the program to stdout,
 * and waits for the user to press enter
 */
void displayGreeting(void);

/**
 * This function calculates the difference in seconds between to timeval
 * structs.
 *
 * struct timeval start: The earlier time
 * struct timeval end: The later time
 * returns: The elapsed time, in seconds
 */
double elapsedTime(struct timeval start, struct timeval end);

/**
 * Generates a random 6 character id, consisting of two capital letters, 
 * followed by four numerals.
 * The id is checked to for uniqueness against the id's of sheep currently
 * in the shed.
 *
 * returns: A 6 character null-terminated string
 */
char * generateId();

/**
 * Prints the id of every sheep in the shed, and the amount of time they have
 * been there to stdout
 */
void printInfo(void);

/**
 * There is a chance that this function will try to create a new sheep and put
 * it into the shed every SLEEP_TIME microseconds.
 * If it succeeds, it prints a message stating which holding pen the new
 * sheep was put into.
 * It loops until the global variable running is set to false.
 *
 * void * args: a pointer to a double that represents the chance of putting 
 * 				in a new sheep
 * returns: NULL
 */
void * produce(void * args);

/**
 * Sets the global variable running to false, causing produce(), consume()
 * and watch() to terminate.
 */
void quit(void);

/**
 * Waits for user input on stdin.
 * When the user enters p or P, printInfo() is called.
 * When the user enters q or Q, quit() is called.
 *
 * void * args: (ignored)
 * returns: NULL
 */
void * watch(void* args);

/**
 * The amount of time that produce() and consume() will wait between attempts,
 * in microseconds.
 */
#define SLEEP_TIME 500000u

//The structure containing the sheep
Shed * shed;
//A semaphore representing the number of free spaces in the shed
sem_t shedEmpty;
//A semaphore representing the number of sheep in the shed
sem_t shedFull;
//A mutex lock, guarding access to the shed
pthread_mutex_t shedAccess;
//A flag variable that tells the threads when to terminate
bool running;

int main(int argc, char** argv) {
    pthread_t producer;
    pthread_t consumer;
    pthread_t monitor;
    
    double arrivalChance;
    double departureChance;
    
    //Check for correct program arguments
    int error = 0;
    
    if(argc == 1) {
        arrivalChance = 0.6;
        departureChance = 0.4;
    } else if (argc == 3) {
        arrivalChance = atof(argv[1]);
        departureChance = atof(argv[2]);
    } else {
        fprintf(stderr, "Usage: simulator [arrival-chance departure-chance]\n");
        exit(EXIT_FAILURE);
    }
    
    //Initialise chances and shed
    
    shed = malloc(sizeof(Shed));
    if(shed == NULL) {
        fprintf(stderr, "Failed to allocate Shed");
        exit(EXIT_FAILURE);
    }
    
    //Initialise mutex and semaphores
    error |= sem_init(&shedEmpty, 0, MAX_SHEEP);
    error |= sem_init(&shedFull, 0, 0);
    error |= pthread_mutex_init(&shedAccess, NULL);
    if(error) {
        fprintf(stderr, "Failed to initialise semaphores and/or mutex");
        exit(error);
    }

    //Display intro message and wait for user
    displayGreeting();

    //Start threads
    running = true;

    error |= pthread_create(&producer, NULL, &produce, &arrivalChance);
    error |= pthread_create(&consumer, NULL, &consume, &departureChance);
    error |= pthread_create(&monitor, NULL, &watch, NULL);
    if(error) {
        fprintf(stderr, "Error creating threads");
        exit(error);
    }

    //Wait for threads to finish
    error |= pthread_join(producer, NULL);
    error |= pthread_join(consumer, NULL);
    error |= pthread_join(monitor, NULL);
    if(error) {
        fprintf(stderr, "Error joining threads");
        exit(error);
    }
    
    exit(EXIT_SUCCESS);
}

//Consumer function
void * consume(void * args) {
    double * departureChance = (double *) args;
    struct timeval exited;

    while(running){
        usleep(SLEEP_TIME);
        if(rand() / ((double)RAND_MAX) > *departureChance){
            continue;
        }
        if(sem_trywait(&shedFull)){
            printf("Shed is Empty\n");
            continue;
        }

        pthread_mutex_lock(&shedAccess);

        int pen = rand() % shed->length;
        Sheep * poppedSheep = popSheep(shed, pen);
        gettimeofday(&exited, NULL);

        pthread_mutex_unlock(&shedAccess);
        sem_post(&shedEmpty);

        printf("Sheep %s departed holding pen %d and stayed %.2f seconds.\n", poppedSheep->id, pen+1,
            elapsedTime(poppedSheep->entered, exited));
    }
    return NULL;
}

void displayGreeting(void) {
    char c;

    printf("\
            Welcome to the shearing shed simulator.\n\n\
            Press p or P followed by return to display the state of\n\
            the shed. Press q or Q followed by return to terminate the\n\
            simulation.\n\n\
            Press return to start the simulation.\n");
    
    do {
        c = getchar();
    } while (c != '\n');
}

double elapsedTime(struct timeval start, struct timeval end) {
    return (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec)/1000000.0;
}

char * generateId() {
    char * name = calloc(7, sizeof(char));
    
    name[0] = (char)(( rand() % 26 ) + 65 );  //Capital letters are between 65 and 90
    name[1] = (char)(( rand() % 26 ) + 65 ); 
    
    for( int i = 2; i < 6; ++i) {
        name[i] = (char)( rand() % 10 + 48 ); //Numerals are between 48 and 57
    }
    
    name[6] = (char)0; //Add null terminator
    
	//Make sure the name is unique within the shed,
	//The chance of a collision is about 1 in 750,000, which is not inconsiderable
	//The chance of two consecutive ids is about 1 in 141,000,000,000, so we should be safe
	pthread_mutex_lock(&shedAccess);
	for(int i = 0; i < shed->length; ++i) {
		if(!strcmp(name, getSheep(shed, i)->id)) {
			name[5] = ((name[5] + 1) - 48) % 10 + 48; //increment the last character of the name
		}
	}
	pthread_mutex_unlock(&shedAccess);
    return name;
}

void printInfo(void) {
    Sheep * sheep;
    struct timeval now;

    printf("Shearing shed status:\n");
    pthread_mutex_lock(&shedAccess);

    gettimeofday(&now, NULL);

    for(int i = 0; i < MAX_SHEEP; ++i) {

        sheep = getSheep(shed, i);

        printf("%d: ", i+1);
        if(sheep == NULL) {
            printf("Empty\n");
        } else {
            printf("%s (docked for %.2f seconds)\n", sheep->id,
                elapsedTime(sheep->entered, now) );
        }
    }

    pthread_mutex_unlock(&shedAccess);

}

//Producer function
void * produce(void * args) {
    double * arrivalChance = (double *) args;
    while(running){
        usleep(SLEEP_TIME);
        if(rand() / ((double)RAND_MAX) > *arrivalChance){
            continue;
        }
        if(sem_trywait(&shedEmpty)){
            printf("Shed is full\n");
            continue;
        }

        char * randID = generateId();
        Sheep * sheep = malloc(sizeof(Sheep));
        sheep->id = randID;
        
        pthread_mutex_lock(&shedAccess);

        int hPen = appendSheep(shed, sheep);
        gettimeofday(&sheep->entered, NULL);

        pthread_mutex_unlock(&shedAccess);
        sem_post(&shedFull);

        printf("Sheep %s arrived in holding pen %d\n", randID, hPen+1);
    }
    return NULL;    
}

//
void quit(void) {
    running = false;
}

//Monitor function
void * watch(void * args) {
    char c;

    do {
        c = getchar();

        switch(c) {
            case('p'):
            case('P'):
                printInfo();
                break;
            case('q'):
            case('Q'):
                quit();
                break;
        }
    }
    while (running);
    
    return NULL;
}
